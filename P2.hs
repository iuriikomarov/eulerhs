fibonacci :: [Integer]
fibonacci = 0 : 1 :
    [ n |
        x <- [2..],
        let n = ((fibonacci !! (x-1)) + (fibonacci !! (x-2)))
    ]

solution :: Integer -> Integer
solution n =
    sum $ filter even list
    where
        list = takeWhile (\x -> x <= n) fibonacci

main :: IO ()
main = do
    print $ solution 4000000
