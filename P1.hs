solution :: Integer -> Integer
solution n =
    sum $ filter isMod [1..n]
    where
        isMod x = x `mod` 3 == 0 || x `mod` 5 == 0

main :: IO ()
main = do
    print $ solution 999
